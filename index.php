<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'bootstrap.php';

use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Middleware\Logging as ChatterLogging;

$app = new \Slim\App();
$app->add(new ChatterLogging());

$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});

$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();
    $payload = [];
    foreach($messages as $msg){
         $payload[$msg->id] = [
             'body'=>$msg->body,
             'user_id'=>$msg->user_id,
             'created_at'=>$msg->created_at
         ];
    }
    return $response->withStatus(200)->withJson($payload);
 });
 $app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid =$request->getParsedBodyParam('userid','');
    $_message = new Message();
    $_message->body =$message;
    $_message->user_id = $userid;
    $_message->save();
    if($_message->id){
        $payload = ['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
    }else{
        return $response->withStatus(400);
    }

    

});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']);
    $message->delete();
    if($message->exists){
        return $response->withStatus(400);
    }else{
        return $response->withStatus(200);
    }  

});

$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
   
    $_message =  Message::find($args['message_id']);
    die("message id is".$_message->id);
    $_message->body = $message;
    if($_message->save()){
        $payload = ['message_id'=>$_message->id, "resault"=>"The message has benn updated successfuly"];
          return $response->withStatus(200)->withJson($payload);
    }else{
     return $response->withStatus(400);
    }
});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    Message::insert($payload);
      return $response->withStatus(201)->withJson($payload);
});

 $app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
         $payload[$usr->id] = [
             'id'=>$usr->id,
             'username'=>$usr->username,
             'email'=>$usr->email
         ];
    }
    return $response->withStatus(200)->withJson($payload);
 });
 
 $app->post('/users', function($request, $response,$args){
     $user = $request->getParsedBodyParam('user','');
     $mail = $request->getParsedBodyParam('mail','');
     $_user = new user(); //create new user
     $_user->username = $user;
     $_user->email = $mail;
     $_user->save();
     if($_user->id){
         $payload = ['user_id'=>$_user->id];
         return $response->withStatus(201)->withJson($payload);
     } else {
         return $response->withStatus(400);
     }
    });
 
 
 $app->delete('/users/{user_id}', function($request, $response,$args){
     $user = User::find($args['user_id']);
     $user->delete();
     if($user->exists){
         return $response->withStatus(400);
     }else{
         return $response->withStatus(200);
     }
});

$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    User::insert($payload);
      return $response->withStatus(201)->withJson($payload);
});

$app->delete('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    User::find($payload);
    $payload->delete();
    return $response->withStatus(201)->withJson($payload);
});
$app->run();